
<?php

class usuarioController {

    private $_usuarios;
    public function __construct() {
        $this->_usuarios = new Usuarios();
    }

    public function listar() {
        return  $this->_usuarios->findAll();
    }
  
    public function busca($dados) {
        return  $this->_usuarios->find($dados);
    }
    
    public function buscaID($id) {
        return  $this->_usuarios->findByID($id);
    }

    public function apagar($id) {
        return  $this->_usuarios->remove($id);
    }
    
    public function salvar($dados) {
        $usuario = new Usuario();
        $usuario->setNome($dados['nome']);
        $usuario->setEmail($dados['email']);
        $usuario->setSenha($dados['senha']);
        
        if(isset($dados['id']) && $dados['id'] > 0){
            $usuario->setId($dados['id']);
            return  $this->_usuarios->update($usuario);
        }else{
           
            return  $this->_usuarios->insert($usuario);
        }
    }
    public function senha($dados) {
        $usuario = new Usuario();
        $usuario->setSenha($dados['senha']);
        
        if(isset($dados['id']) && $dados['id'] > 0){
            $usuario->setId($dados['id']);
            return  $this->_usuarios->newPass($usuario);
        }
    }
}

