
<?php

class filmeController {

    private $_filmes;
    
    public function __construct() {
        $this->_filmes = new Filmes();
    }

    public function listar() {
        return  $this->_filmes->findAll();
    }
  
    public function busca($dados) {
        return  $this->_filmes->find($dados);
    }
    
    
    public function buscaID($id) {
        return  $this->_filmes->findByID($id);
    }

    public function apagar($id) {
        return  $this->_filmes->remove($id);
    }
    
    public function salvar($dados) {
        $filme = new Filme();
        $filme->setNome($dados['nome']);
        $filme->setResumo($dados['resumo']);
        $filme->setSinopse($dados['sinopse']);
        $filme->setDestaque($dados['destaque']);
        $filme->setQuantidade($dados['quantidade']);
        $filme->setCategoria($dados['id_categoria']);
        $filme->setStatus($dados['status']);
        
        if(isset($dados['id']) && $dados['id'] > 0){
            $filme->setId($dados['id']);
            return  $this->_filmes->update($filme);
        }else{
           
            return  $this->_filmes->insert($filme);
        }
    }
    public function locar($dados) {
         
         if(isset($dados['id_filme']) && $dados['id_cliente'] > 0){
              return  $this->_filmes->locar($dados);
        }
    }
    
    public function categoria() {
        $categoria = new Categorias();
        return  $categoria->findAll();
    }
    
    public function clientes() {
        $clientes = new Clientes();
        return  $clientes->findAll();
    }
    public function locacoes($dados){
        return $this->_filmes->findLocacao($dados);
    }
    
    public function clienteLocacoes($dados){
        return $this->_filmes->clienteLocacao($dados);
    }
    
     public function baixar($idFilme){
        return $this->_filmes->baixar($idFilme);
    }
    
}

