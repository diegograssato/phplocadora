<!DOCTYPE html>
<html>
    <head>

        <title>..::Locadora Vende Mais::..</title>
        <!-- proper charset -->
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
        <link rel="stylesheet" type="text/css" href="/locadora/public/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" type="text/css" href="/locadora/public/css/style.css" />
        <link rel="stylesheet" type="text/css" href="/locadora/public/css/bootstrap.min.css" />
        <script type="text/javascript" src="/locadora/public/js/html5.js"></script>
        <script type="text/javascript" src="/locadora/public/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/locadora/public/js/jquery.min.js"></script>

        <link rel="shortcut icon" href="/locadora/public/images/favicon.ico" />
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php">Locadora</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li class="active"><a href="index.php?control=filmes&pag=locacao">Locações</a></li>
                            <li class="active"><a href="index.php?control=filmes&pag=list">Filmes</a></li>
                            <li class="active"><a href="index.php?control=categorias&pag=list">Categorias</a></li>
                            <li class="active"><a href="index.php?control=clientes&pag=list">Clientes</a></li>
                            <li class="active"><a href="index.php?control=usuarios&pag=list">Usuários</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container">
            <?php echo $pagemaincontent; ?>
            <hr>

        </div> <!-- /container -->
        <div align="center">
            <footer>
                <p>&copy; 2012 - <?php echo date("Y"); ?> by DTuX Ltda. </p>
            </footer>
        </div>
    </body>
</html>