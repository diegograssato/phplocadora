<?php
$filmes = new filmeController();
if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['listar']))) {
    $filme = $filmes->buscaID($_GET['listar']);
    ?>
    <div class="hero-unit">
        <table style="width: 80%;">
            <tr><td>Nome</td><td><?= $filme['nome']; ?></td></tr>
            <tr><td>Resumo</td><td><?= $filme['resumo']; ?></td></tr>
            <tr><td>Sinopse</td><td><?= $filme['sinopse']; ?></td></tr>
            <tr><td>Quantidade</td><td><?= $filme['quantidade']; ?></td></tr>
            <tr><td>Destaque</td><td><?= $filme['destaque'] == 1? 'Sim':'Não'; ?></td></tr>
            <tr><td>Status</td><td><?= $filme['status'] == 1? 'Ativo':'Inativo'; ?></td></tr>
                <tr><td>Cadastrado</td><td><?php echo date("d/m/Y", strtotime($filme['criacao'])); ?></td></tr>
            <tr>
                <td> 
                    <a href="index.php?control=filmes&pag=list">Voltar </a> 
                </td>
                <td> 
                    <a href="index.php?control=filmes&pag=zform&id=<?= $filme['id']; ?>">Editar</a>&emsp;&emsp;
                    <a href="index.php?control=filmes&pag=list&del=<?= $filme['id']; ?>">Remover</a>
                </td>
            </tr> 
        </table>
    </div>
<?php } ?>