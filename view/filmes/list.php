<?php
$filme = new filmeController();
$lista = null;

if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['del']))) {

    if ($filme->apagar($_GET['del']) == true) {
        header('Location: index.php?control=filmes&pag=list&msg=Removido com sucesso!');
    } else {
        header('Location: index.php?control=filmes&pag=list&msg=Impossivel Remover!');
    }
}

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST['busca']))) {
    $lista = $filme->busca($_POST['busca']);
} else {
    $lista = $filme->listar();
}
?>
<fieldset>
    <legend>Listagem de Filmes </legend>
    <div class="input-append">
        <form id="cadastro" method="post" action="#">
            <input class="span2" style="width: 550px"  id="appendedInputButtons" type="text"name="busca" type="text" placeholder="Buscar">
            <input class="btn" type="submit" value="Buscar"/>
            <a href="index.php?control=filmes&pag=zform" class="btn btn-primary">Novo Filme</a>
        </form>

    </div>
    <br>
</fieldset>
<table style="width: 100%;text-align: center" class="table table-hover">
    <!-- Segunda linha -->
    <tr>
        <th> Nome </th>
        <th> Resumo </th>
        <th>  Criação </th>
        <th>&emsp; Opções </th>
    </tr>
    <!-- Lista todos os itens e os itens de busca -->
    <?php foreach ($lista as $filme): //print_r($contato);  ?>
        <tr>
            <td><?= $filme['nome']; ?></td>
            <td><?= $filme['resumo']; ?></td>
            <td>&ensp;<?php echo date("d/m/Y", strtotime($filme['criacao'])); ?></td>
            <td><div class="btn-group">
                    <a class="btn" href="index.php?control=filmes&pag=details&listar=<?= $filme['id']; ?>"><i class="icon-align-left"></i>Detalhes</a>&emsp;
                    <a class="btn" href="index.php?control=filmes&pag=zform&id=<?= $filme['id']; ?>">Editar</a>&emsp;
                    <a class="btn" href="index.php?control=filmes&pag=list&del=<?= $filme['id']; ?>">Remover</a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>