<?php
$filme = new filmeController();
$lista = null;

 

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST['busca']))) {
    $lista = $filme->locacoes($_POST['busca']);
} else {
    $lista = $filme->locacoes();
}
?>
<fieldset>
    <legend>Locações </legend>
    <div class="input-append">
        <form id="cadastro" method="post" action="#">
            <input class="span2" style="width: 550px"  id="appendedInputButtons" type="text"name="busca" type="text" placeholder="Buscar">
            <input class="btn" type="submit" value="Buscar"/>
           
        </form>

    </div>
    <br>
</fieldset>
<table style="width: 100%;text-align: center" class="table table-hover">
    <!-- Segunda linha -->
    <tr>
        <th> Nome </th>
        <th>  Quantidade </th>
        <th>  Livres </th>
        <th>  Locados </th>
        <th>&emsp; Opções </th>
    </tr>
    <!-- Lista todos os itens e os itens de busca -->
    <?php foreach ($lista as $filme): //print_r($contato);  ?>
        <tr>
            <td><?= $filme['nome']; ?></td>
            <td><?= $filme['quantidade']; ?></td>
            <td><?= $filme['livre']; ?></td>
            <td><?= $filme['qLocados']; ?></td>
            <td>
                <?php if($filme['livre'] > 0){?>
                    <div class="btn-group">
                        <a class="btn" href="index.php?control=filmes&pag=locar&id=<?= $filme['id']; ?>"><i class="icon-align-left"></i>Locar&emsp;</a>
                    </div>
                <?php } ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>