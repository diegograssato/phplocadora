<?php
$contatos = new clienteController();
if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['listar']))) {
    $contato = $contatos->buscaID($_GET['listar']);
    ?>
    <div class="hero-unit">
        <table style="width: 80%;">
            <tr><td>Nome</td><td><?= $contato['nome']; ?></td></tr>
            <tr><td>E-mail</td><td><?= $contato['email']; ?></td></tr>
            <tr><td>Data de Cadastro</td><td><?php echo date("d/m/Y", strtotime($contato['criacao'])); ?></td></tr>
            <tr>
                <td> 
                    <a href="index.php?control=clientes&pag=list">Voltar </a> 
                </td>
                <td> 
                    <a href="index.php?control=clientes&pag=zform&id=<?= $contato['id']; ?>">Editar</a>&emsp;&emsp;
                    <a href="index.php?control=clientes&pag=list&del=<?= $contato['id']; ?>">Remover</a>
                </td>
            </tr> 
        </table>
    </div>
<?php } ?>