<?php
$contatos = new clienteController();

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST))) {
    $salvar = $contatos->salvar($_POST);
    if ($salvar > 0) {
        header('Location: index.php?control=clientes&pag=list&msg=Registro ' . $salvar . ' alterado/criado com sucesso');
    } else {
        header('Location: index.php?control=clientes&pag=list&msg=Falha ao alterar');
    }
} else {
    if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['id']))) {
        $id = $_GET['id'];
        $contato = $contatos->buscaID($id);
    }
    ?>

    <label class="subtitle">Informações do Contato</label>
    <form id='cadastro' method='POST' action='#'>
        <table>
            <tr>
                <td>
                    <label for="nome">Nome</label>
                </td>
                <td>
                    <?php
                    if ($id > 0) {
                        echo '<input name="id" type="hidden" value="' . $contato['id'] . '" />';
                    }
                    ?>
                    <input name="nome" type="text" placeholder="Nome" value="<?= $contato['nome'] ?>" />
                </td>
            </tr>

            <tr>
                <td>
                    <label for="email">Email</label>
                </td>
                <td>
                    <input name="email" type="email" placeholder="Email" required value="<?= $contato['email'] ?>" />
                </td>
            </tr>

            <tr>
                <td>
                    <input type='submit' class="btn btn-primary" value=' Salvar '>
                </td>
                <td>
                    <input type='reset' class="btn" name='reset' value='Limpar'>
                </td>
            </tr>
        </table>



    </form>


<?php
}