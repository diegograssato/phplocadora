<?php
$categorias = new categoriaController();
$lista = null;

if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['del']))) {

    if ($categorias->apagar($_GET['del']) == true) {
        header('Location: index.php?control=categorias&pag=list&msg=Removido com sucesso!');
    } else {
        header('Location: index.php?control=categorias&pag=list&msg=Impossivel Remover!');
    }
}

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST['busca']))) {
    $lista = $categorias->busca($_POST['busca']);
} else {
    $lista = $categorias->listar();
}
?>
<fieldset>
    <legend>Listagem de Categorias  </legend>
    <div class="input-append">
        <form id="cadastro" method="post" action="#">
            <input class="span2" style="width: 550px"  id="appendedInputButtons" type="text"name="busca" type="text" placeholder="Buscar">
            <input class="btn" type="submit" value="Buscar"/>
            <a href="index.php?control=categorias&pag=zform" class="btn btn-primary">Nova Categoria</a>
        </form>

    </div>
    <br>
</fieldset>
<table style="width: 100%;text-align: center" class="table table-hover">
    <!-- Segunda linha -->
    <tr>
        <th> Nome </th>
        <th> Descrição </th>
        <th> Data de Criação </th>
        <th>&emsp; Opções </th>
    </tr>
    <!-- Lista todos os itens e os itens de busca -->
    <?php foreach ($lista as $contato): //print_r($contato);  ?>
        <tr>
            <td><?= $contato['nome']; ?></td>
            <td><?= $contato['descricao']; ?></td>
            <td>&ensp;<?php echo date("d/m/Y", strtotime($contato['criacao'])); ?></td>
            <td><div class="btn-group">
                    <a class="btn" href="index.php?control=categorias&pag=details&listar=<?= $contato['id']; ?>"><i class="icon-align-left"></i>Detalhes</a>&emsp;
                    <a class="btn" href="index.php?control=categorias&pag=zform&id=<?= $contato['id']; ?>">Editar</a>&emsp;
                    <a class="btn" href="index.php?control=categorias&pag=list&del=<?= $contato['id']; ?>">Remover</a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>