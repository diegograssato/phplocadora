<?php
$usuarios = new usuarioController();

if (($_SERVER['REQUEST_METHOD'] == 'POST') && (!empty($_POST))) {
    $salvar = $usuarios->salvar($_POST);
    if ($salvar > 0) {
        header('Location: index.php?control=usuarios&pag=list&msg=Registro ' . $salvar . ' alterado/criado com sucesso');
    } else {
        header('Location: index.php?control=usuarios&pag=list&msg=Falha ao alterar');
    }
} else {
    if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['id']))) {
        $id = $_GET['id'];
        $usuario = $usuarios->buscaID($id);
    }
    ?>

    <label class="subtitle">Informações do Contato</label>
    <form id='cadastro' method='POST' action='#'>
        <table>
            <tr>
                <td>
                    <label for="nome">Nome</label>
                </td>
                <td>
                    <?php
                    if ($id > 0) {
                        echo '<input name="id" type="hidden" value="' . $usuario['id'] . '" />';
                    }
                    ?>
                    <input name="nome" type="text" placeholder="Nome" value="<?= $usuario['nome'] ?>" />
                </td>
            </tr>

            <tr>
                <td>
                    <label for="email">Email</label>
                </td>
                <td>
                    <input name="email" type="email" placeholder="Email" required value="<?= $usuario['email'] ?>" />
                </td>
            </tr>
            <?php if ($id == 0) { ?>
                <tr>
                    <td>
                        <label for="senha">Senha</label>
                    </td>
                    <td>
                        <input name="senha" type="password" placeholder="Senha" required value="<?= $usuario['senha'] ?>" />
                    </td>
                </tr>
            <?php }
            ?>
            <tr>
                <td>
                    <input type='submit' class="btn btn-primary" value="Salvar">
                </td>
                <td>
                    <input type='reset' class="btn" name='reset' value='Limpar'>
                </td>
            </tr>
        </table>



    </form>


    <?php
}