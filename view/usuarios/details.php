<?php
$usuarios = new usuarioController();
if (($_SERVER['REQUEST_METHOD'] == 'GET') && (!empty($_GET['listar']))) {
    $usuario = $usuarios->buscaID($_GET['listar']);
    ?>
    <div class="hero-unit">
        <table style="width: 80%;">
            <tr><td>Nome</td><td><?= $usuario['nome']; ?></td></tr>
            <tr><td>E-mail</td><td><?= $usuario['email']; ?></td></tr>
            <tr><td>Data de Cadastro</td><td><?php echo date("d/m/Y", strtotime($usuario['criacao'])); ?></td></tr>
            <tr>
                <td> 
                    <a href="index.php?control=usuarios&pag=list">Voltar </a> 
                </td>
                <td> 
                    <a href="index.php?control=usuarios&pag=zform&id=<?= $usuario['id']; ?>">Editar</a>&emsp;&emsp;
                    <a href="index.php?control=usuarios&pag=list&del=<?= $usuario['id']; ?>">Remover</a>
                </td>
            </tr> 
        </table>
    </div>
<?php } ?>