 
<?php
/**
 * Modelo 
 */
class Categorias {

    /**
     *  Construtor Principal da Class, é o inicio da classe
     */
    private $_db=NULL;
    
    function __construct() {
        $this->_db = new conexao();          
    }

    /**
     * Insere os dados do contato
     * @param type $data[]
     * @return id
     */ 
    public function insert($categoria) {
        // logica para salvar contato no banco
        $sql = "INSERT INTO categoria (nome,descricao)  
                    VALUES (?,?)";
        $query = $this->_db->prepare($sql);
        $query->bindParam(1, $categoria->getNome() , PDO::PARAM_STR);
        $query->bindParam(2, $categoria->getDescricao(), PDO::PARAM_STR);
        $executa = $query->execute();
        if($executa){
            return   $this->_db->lastInsertId();
        }else{
            return false;
        } 
    }

    /**
     * Atualza os dados do contato
     * @param type $data[]
     * @return id
     */
    public function update($categoria) {
        $sql = "UPDATE categoria SET nome=:nome,descricao=:descricao
                    WHERE id=:id";
        $query = $this->_db->prepare($sql);
        $query->bindParam(":id", $categoria->getId() , PDO::PARAM_INT);
        $query->bindParam(":nome", $categoria->getNome() , PDO::PARAM_STR);
        $query->bindParam(":descricao", $categoria->getDescricao() , PDO::PARAM_STR);
        $executa = $query->execute();
        if($executa){
            return $categoria->getId();
        }else{
            return false;
        }
    }

    /**
     * Remove um contato
     * @param type $id
     * @return boolean
     */
    public function remove($id) {
        // logica para remover cliente do banco
        $query = $this->_db->prepare("DELETE FROM categoria WHERE id=?");
        $query->bindParam(1, $id , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            return true ;
        }else{
            return false;
        }
    }
    
   /**
    * Faz a busca de um contato pelo seu ID
    * @param type $id
    * @return data[]
    */
   public function findByID($id) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM categoria WHERE id=?");
        $query->bindParam(1, $id , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetch();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
      
    }
    
    /**
     * Lista todos os contatos
     * @return data[]
     */
    public function findAll() {
        // logica para listar toodos os clientes do banco
        $query =  $this->_db->query("SELECT * FROM categoria")->fetchAll();
        return $query;
    }
    
    /**
     * @param type $dados
     * @return data[]
     */
    public function find($dados) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM categoria WHERE 
                                 (nome LIKE ? 
                                OR descricao LIKE ? )");
        $query->bindValue(1, "%$dados%", PDO::PARAM_STR);
        $query->bindValue(2, "%$dados%", PDO::PARAM_STR);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetchAll();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
    }

}

?>
