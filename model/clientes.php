 
<?php
/**
 * Modelo 
 */
class clientes {

    /**
     *  Construtor Principal da Class, é o inicio da classe
     */
    private $_db=NULL;
    
    function __construct() {
        $this->_db = new conexao();          
    }

    /**
     * Insere os dados do contato
     * @param type $data[]
     * @return id
     */ 
    public function insert($cliente) {
        // logica para salvar contato no banco
        $sql = "INSERT INTO clientes (nome,email,nivel)  
                    VALUES (?,?,?)";
        $query = $this->_db->prepare($sql);
        $query->bindParam(1, $cliente->getNome() , PDO::PARAM_STR);
        $query->bindParam(2, $cliente->getEmail() , PDO::PARAM_STR);
        $query->bindParam(3, $cliente->getNivel() , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            return   $this->_db->lastInsertId();
        }else{
            return false;
        } 
    }

    /**
     * Atualza os dados do contato
     * @param type $data[]
     * @return id
     */
    public function update($cliente) {
        $sql = "UPDATE clientes SET nome=:nome,
                    email=:email WHERE id=:id";
        $query = $this->_db->prepare($sql);
        $query->bindParam(":id", $cliente->getId() , PDO::PARAM_INT);
        $query->bindParam(":nome", $cliente->getNome() , PDO::PARAM_STR);
        $query->bindParam(":email", $cliente->getEmail() , PDO::PARAM_STR);
        $executa = $query->execute();
        if($executa){
            return $cliente->getId();
        }else{
            return false;
        }
    }

    /**
     * Remove um contato
     * @param type $id
     * @return boolean
     */
    public function remove($id) {
        // logica para remover cliente do banco
        $query = $this->_db->prepare("DELETE FROM clientes WHERE id=?");
        $query->bindParam(1, $id , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            return true ;
        }else{
            return false;
        }
    }
    
   /**
    * Faz a busca de um contato pelo seu ID
    * @param type $id
    * @return data[]
    */
   public function findByID($id) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM clientes WHERE id=?");
        $query->bindParam(1, $id , PDO::PARAM_INT);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetch();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
      
    }
    
    /**
     * Lista todos os contatos
     * @return data[]
     */
    public function findAll() {
        // logica para listar toodos os clientes do banco
        $query =  $this->_db->query("SELECT * FROM clientes")->fetchAll();
        return $query;
    }
    
    /**
     * @param type $dados
     * @return data[]
     */
    public function find($dados) {
        // logica para listar toodos os clientes do banco
        $query = $this->_db->prepare("SELECT * FROM clientes WHERE 
                                 nome LIKE ? 
                                OR email LIKE ? ");
        $query->bindValue(1, "%$dados%", PDO::PARAM_STR);
        $query->bindValue(2, "%$dados%", PDO::PARAM_STR);
        $executa = $query->execute();
        if($executa){
            $rows = $query->fetchAll();
        }else{
           echo 'Erro ao bucar os dados';
       }
       return $rows;
    }

}

?>
