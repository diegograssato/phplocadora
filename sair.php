
<?php
session_start();
unset($_SESSION['usuario']);
unset($_SESSION['logado_em']);
session_destroy();
header('Location: /agenda/login.php?msg=Sua sessão foi finalizada!');
?>

