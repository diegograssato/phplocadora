<?php
 
function __autoload($class_name) {
    
    $path = $_SERVER['DOCUMENT_ROOT'] . "/".APP;
    $array = array("controller", "model", "model/entity", "config");
    foreach ($array as $dir) {
        if (file_exists($path . "/" . $dir . "/{$class_name}.php")) {
            require_once $path . "/" . $dir . "/{$class_name}.php";
        }
    }
    
}